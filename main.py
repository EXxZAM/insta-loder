
# Aval package instaloader ro dar terminal nasb konid =>
# pip install instaloader
import instaloader

# Get instance
L = instaloader.Instaloader()

# Login or load session
L.login('username', 'password')        # (login) username va password khodeton ro inja vared konid


# Obtain profile metadata
profile = instaloader.Profile.from_username(L.context, 'person') # be jaye person id shakhs mored nazar be onvan mesal => billieeilish

# Print list of followees
for followee in profile.get_followees():
    print(followee.username) # tamam following haye shakhs mored nazaro print mikne
    