# Insta-Loader
📝
با این پکیج به راحتی میتوانید از اینستاگرام دانلود کنید و یا اطلاعات متای کاربری را دریافت کنید


## Needed Packages 

🐍
```bash
pip install instaloader
```


## Made By ❤
💻This python project is made by Seyed Mahdi Olamaei aka exxzam


🔗 [instagram.com/mahdi12ad](https://instagram.com/mahdi12ad)


🔗 [instagram.com/python.mahdi](https://instagram.com/python.mahdi)


🔗 [t.me/exxzam](https://t.me/exxzam)



## Website

🌐 [EXxZAM Tutorials](https://exxzamtutorials.ir)

